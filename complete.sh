#!/bin/bash

set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

sudo sed -i 's/#\[multilib]/[multilib]/g' /etc/pacman.conf
sudo sed -i '/\[multilib]/!b;n;c Include = \/etc\/pacman.d\/mirrorlist/' /etc/pacman.conf
sudo pacman -Syu

sudo pacman -S --noconfirm ntfs-3g openssh\
               nvidia git plasma-desktop mpv mupdf qbittorrent konsole \
               dolphin network-manager-applet sddm code \
               archiso plasma-pa firefox base-devel firefox-ublock-origin kscreen \
	            plasma-nm xournalpp man android-tools p7zip wget ark libreoffice \
               cups nss-mdns sane texlive-core texstudio unzip nodejs npm \
               steam lutris python-pip intellij-idea-community-edition samba \
               libvirt qemu ebtables dnsmasq bridge-utils openbsd-netcat \
               virt-manager edk2-ovmf

sudo systemctl enable sddm

git clone https://github.com/pyllyukko/user.js
pushd user.js
make systemwide_user.js
sudo cp systemwide_user.js /usr/lib/firefox/mozilla.cfg
popd
cat << EOF > local-settings.js
pref("general.config.obscure_value", 0);
pref("general.config.filename", "mozilla.cfg");
EOF
sudo cp local-settings.js /usr/lib/firefox/defaults/pref/

wget https://raw.githubusercontent.com/mpv-player/mpv/master/TOOLS/lua/autoload.lua
mkdir -p ~/.config/mpv/scripts
cp autoload.lua ~/.config/mpv/scripts

sudo systemctl enable cups.service
sudo systemctl enable avahi-daemon.service
sudo sed -i 's/hosts: files mymachines myhostname resolve \[!UNAVAIL=return\] dns/hosts: files mymachines myhostname mdns_minimal \[NOTFOUND=return\] resolve \[!UNAVAIL=return\] dns/g' /etc/nsswitch.conf

wget https://github.com/Colocasian/ungoogled-chromium-binaries/releases/download/89.0.4389.90-3/ungoogled-chromium-89.0.4389.90-3-x86_64.pkg.tar.zst
sudo pacman -U ungoogled-chromium-89.0.4389.90-3-x86_64.pkg.tar.zst

wget https://github.com/Genymobile/gnirehtet/releases/download/v2.5/gnirehtet-rust-linux64-v2.5.zip
mkdir Applications
unzip gnirehtet-rust-linux64-v2.5.zip
sudo cp -r gnirehtet-rust-linux64 /opt
cat << EOF > gnirehtet.desktop
[Desktop Entry]
Type=Application
Name=gnirehtet
Categories=Network;
Path=/opt/gnirehtet-rust-linux64/
Exec=/opt/gnirehtet-rust-linux64/gnirehtet run
EOF
sudo cp gnirehtet.desktop /usr/share/applications

sudo npm install -g nativefier

nativefier web.whatsapp.com
sudo cp -r WhatsAppWeb-linux-x64 /opt
cat << EOF > whatsapp.desktop
[Desktop Entry]
Type=Application
Name=WhatsApp
Categories=Network;
Path=/opt/WhatsAppWeb-linux-x64/
Exec=/opt/WhatsAppWeb-linux-x64/WhatsAppWeb
Icon=/opt/WhatsAppWeb-linux-x64/resources/app/icon.png
EOF
sudo cp whatsapp.desktop /usr/share/applications

sudo pip install spotdl
sudo pip install youtube-dl

sudo pacman -S nvidia-dkms nvidia-utils lib32-nvidia-utils nvidia-settings vulkan-icd-loader lib32-vulkan-icd-loader
sudo pacman -S wine-staging giflib lib32-giflib libpng lib32-libpng libldap lib32-libldap gnutls lib32-gnutls mpg123 lib32-mpg123 openal lib32-openal v4l-utils lib32-v4l-utils libpulse lib32-libpulse libgpg-error lib32-libgpg-error alsa-plugins lib32-alsa-plugins alsa-lib lib32-alsa-lib libjpeg-turbo lib32-libjpeg-turbo sqlite lib32-sqlite libxcomposite lib32-libxcomposite libxinerama lib32-libgcrypt libgcrypt lib32-libxinerama ncurses lib32-ncurses opencl-icd-loader lib32-opencl-icd-loader libxslt lib32-libxslt libva lib32-libva gtk3 lib32-gtk3 gst-plugins-base-libs lib32-gst-plugins-base-libs vulkan-icd-loader lib32-vulkan-icd-loader

git clone https://git.samba.org/samba.git/
cat << EOF >> samba/examples/smb.conf.default
[tmp]
   comment = Temporary file space
   path = /tmp
   read only = no
   public = yes
EOF
sudo cp samba/examples/smb.conf.default /etc/samba/smb.conf
sudo systemctl enable smb

sudo systemctl enable --now libvirtd
sudo systemctl enable --now virtlogd
sudo virsh net-autostart default
sudo virsh net-start default