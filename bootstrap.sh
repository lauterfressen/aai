#!/bin/bash

set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

pacman -Sy --noconfirm pacman-contrib dialog git wget

devicelist=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
device=$(dialog --stdout --menu "Select installtion disk" 0 0 0 ${devicelist}) || exit 1
clear

echo "Updating mirror list"
reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist

### Get infomation from user ###
hostname=$(dialog --stdout --inputbox "Enter hostname" 0 0) || exit 1
clear
: ${hostname:?"hostname cannot be empty"}

user=$(dialog --stdout --inputbox "Enter admin username" 0 0) || exit 1
clear
: ${user:?"user cannot be empty"}

password=$(dialog --stdout --passwordbox "Enter admin password" 0 0) || exit 1
clear
: ${password:?"password cannot be empty"}
password2=$(dialog --stdout --passwordbox "Enter admin password again" 0 0) || exit 1
clear
[[ "$password" == "$password2" ]] || ( echo "Passwords did not match"; exit 1; )

num_boot=5
num_swap=6
num_root=7

sgdisk -n $num_boot:0:+500M -t $num_boot:ef00 $device
sgdisk -n $num_swap:0:+500M -t $num_swap:8200 $device
sgdisk -n $num_root:0:0 -t $num_root:8300 $device

### Set up logging ###
exec 1> >(tee "stdout.log")
exec 2> >(tee "stderr.log")

timedatectl set-ntp true

# Simple globbing was not enough as on one device I needed to match /dev/mmcblk0p1 
# but not /dev/mmcblk0boot1 while being able to match /dev/sda1 on other devices.
part_boot="$(ls ${device}* | grep -E "^${device}p?${num_boot}$")"
part_swap="$(ls ${device}* | grep -E "^${device}p?${num_swap}$")"
part_root="$(ls ${device}* | grep -E "^${device}p?${num_root}$")"

wipefs "${part_boot}"
wipefs "${part_swap}"
wipefs "${part_root}"

mkfs.vfat -F32 "${part_boot}"
mkswap "${part_swap}"
mkfs.ext4 "${part_root}"

swapon "${part_swap}"
mount "${part_root}" /mnt
mkdir /mnt/boot
mount "${part_boot}" /mnt/boot

### Install and configure the basic system ###

pacstrap /mnt base linux linux-firmware vim efibootmgr sudo openssh \
              intel-ucode networkmanager


genfstab -t PARTUUID /mnt >> /mnt/etc/fstab

arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime

arch-chroot /mnt hwclock --systohc

cat << EOF > /mnt/etc/locale.gen
en_US.UTF-8 UTF-8
en_US ISO-8859-1
EOF

arch-chroot /mnt locale-gen

echo "LANG=en_US.UTF-8" > /mnt/etc/locale.conf

echo "${hostname}" > /mnt/etc/hostname

cat << EOF > /mnt/etc/hosts
127.0.0.1	localhost
::1		    localhost
127.0.1.1	$hostname.localdomain	$hostname
EOF

partuuid=$(blkid -s PARTUUID -o value "$part_root")
echo $partuuid
efibootmgr --disk $device --part $num_boot --create --label "Arch Linux" --loader /vmlinuz-linux --unicode "root=PARTUUID=$partuuid rw initrd=\intel-ucode.img initrd=\initramfs-linux.img nvidia-drm.modeset=1" --verbose

arch-chroot /mnt groupadd libvirt
arch-chroot /mnt useradd -m -g wheel -G libvirt "$user"

echo "$user:$password" | chpasswd --root /mnt
echo "root:$password" | chpasswd --root /mnt

arch-chroot /mnt systemctl enable NetworkManager

echo ' %wheel ALL=(ALL) ALL' >> /mnt/etc/sudoers